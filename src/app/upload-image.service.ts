import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UploadImageService {
  // baseApiUrl = "https://file.io";
  baseApiUrl = "https://api.cloudinary.com/v1_1/codermj/upload";
  CLOUDINARY_UPLOAD_PRESET = "n8hb45ip";
  constructor(private http: HttpClient) {}
  upload(file): Observable<any> {
    console.log("upload service is getting called");
    // Create form data
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append("file", file, file.name);
    formData.append("upload_preset", this.CLOUDINARY_UPLOAD_PRESET);

    // Make http post request over api
    // with formData as req
    return this.http.post(this.baseApiUrl, formData);
  }
}
