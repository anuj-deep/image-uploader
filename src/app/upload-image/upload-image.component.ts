import { Component, OnInit } from "@angular/core";
import { UploadImageService } from "../upload-image.service";

@Component({
  selector: "app-upload-image",
  templateUrl: "./upload-image.component.html",
  styleUrls: ["./upload-image.component.css"],
})
export class UploadImageComponent implements OnInit {
  shortLink: string = "";
  loading: boolean = false;
  file: File = null;
  isUploaded: boolean = false;
  constructor(private imageUploadService: UploadImageService) {}

  ngOnInit() {}
  chooseFile(event) {
    this.file = event.target.files[0];
    this.loading = !this.loading;
    console.log("choose file is getting clicked");
    console.log(this.file);
    this.imageUploadService.upload(this.file).subscribe((event: any) => {
      console.log(event);
      if (typeof event === "object") {
        // Short link via api response
        this.shortLink = event.secure_url;

        this.loading = false; // Flag variable
        this.isUploaded = true;
        console.log(this.shortLink);
      }
    });
  }
  onSelect(event) {
    console.log("hello");
    console.log(event.addedFiles[0]);
    this.file = event.addedFiles[0];
    this.loading = !this.loading;
    console.log("choose file is getting clicked");
    console.log(this.file);
    this.imageUploadService.upload(this.file).subscribe((event: any) => {
      console.log(event);
      if (typeof event === "object") {
        // Short link via api response
        this.shortLink = event.secure_url;

        this.loading = false; // Flag variable
        this.isUploaded = true;
        console.log(this.shortLink);
      }
    });
  }
  // copyUrl(inputUrl) {
  //   console.log("link copied Successfully");
  //   console.log(inputUrl["placeholder"]);
  //   inputUrl.select();
  //   document.execCommand("copy");
  //   inputUrl.setSelectionRange(0, 0);
  // }
  copyToClipboard(item) {
    console.log(item);
    item.select();
    document.execCommand("copy");
    document.getSelection().removeAllRanges();
  }
}
