import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MatSliderModule } from "@angular/material/slider";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { NgxDropzoneModule } from "ngx-dropzone";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { ClipboardModule } from "ngx-clipboard";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UploadImageComponent } from "./upload-image/upload-image.component";
import {
  CloudinaryModule,
  CloudinaryConfiguration,
  provideCloudinary,
} from "@cloudinary/angular-5.x";
import { Cloudinary } from "cloudinary-core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [AppComponent, UploadImageComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    CloudinaryModule.forRoot({ Cloudinary }, {
      cloud_name: "dwwdfjkwz",
      api_key: "371828172851115",
      api_secret: "t3P8ti9u_b4jgxoUnxOaJI6bv9Y",
    } as CloudinaryConfiguration),
    BrowserAnimationsModule,
    MatSliderModule,
    DragDropModule,
    NgxDropzoneModule,
    MatProgressBarModule,
    ClipboardModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
